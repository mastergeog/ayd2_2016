# This is an auto-generated Django model module.
# You'll have to do the following manually to clean this up:
#   * Rearrange models' order
#   * Make sure each model has one field with primary_key=True
#   * Remove `managed = False` lines if you wish to allow Django to create, modify, and delete the table
# Feel free to rename the models, but don't rename db_table values or field names.
#
# Also note: You'll have to insert the output of 'django-admin sqlcustom [app_label]'
# into your database.
from __future__ import unicode_literals

from django.db import models


class Actividad(models.Model):
    nombre_actividad = models.CharField(db_column='Nombre_Actividad', max_length=45, blank=True, null=True)  # Field name made lowercase.
    precio = models.FloatField(db_column='precio', blank=True, null=True) # Field name made lowercase.
#Cambia Object por el valor de un campo indicado
    def __str__(self):
	cadena=str(self.nombre_actividad)
	return cadena
    def my_function(self, obj) :
	"""My Custom Title"""
	cadena=str(self.nombre_actividad)
	return cadena	

    my_function.short_description = 'This is the Column Name'    
#-----------------
    class Meta:
        managed = False
        db_table = 'Actividad'


class ActividadEstadio(models.Model):
    estadio = models.ForeignKey('Estadio', db_column='Estadio_id')  # Field name made lowercase.
    actividad = models.ForeignKey(Actividad, db_column='Actividad_id')  # Field name made lowercase.
    def __str__(self):
	cadena=str(self.estadio)
	return cadena
    class Meta:
        managed = False
        db_table = 'Actividad_Estadio'





class Estadio(models.Model):
    nombre_estadio = models.CharField(db_column='Nombre_Estadio', max_length=45, blank=True, null=True)  # Field name made lowercase.
    def __str__(self):
	cadena=str(self.nombre_estadio)
	return cadena
    class Meta:
        managed = False
        db_table = 'Estadio'


class AuthGroup(models.Model):
    name = models.CharField(unique=True, max_length=80)

    class Meta:
        managed = False
        db_table = 'auth_group'


class AuthGroupPermissions(models.Model):
    group_id = models.ForeignKey(AuthGroup)
    permission_id = models.ForeignKey('AuthPermission')

    class Meta:
        managed = False
        db_table = 'auth_group_permissions'
        unique_together = (('group_id', 'permission_id'),)


class AuthPermission(models.Model):
    name = models.CharField(max_length=255)
    content_type_id = models.ForeignKey('DjangoContentType')
    codename = models.CharField(max_length=100)

    class Meta:
        managed = False
        db_table = 'auth_permission'
        unique_together = (('content_type_id', 'codename'),)


class AuthUser(models.Model):
    password = models.CharField(max_length=128)
    last_login = models.DateTimeField(blank=True, null=True)
    is_superuser = models.IntegerField()
    username = models.CharField(unique=True, max_length=30)
    first_name = models.CharField(max_length=30)
    last_name = models.CharField(max_length=30)
    email = models.CharField(max_length=254)
    is_staff = models.IntegerField()
    is_active = models.IntegerField()
    date_joined = models.DateTimeField()
#Cambia Object por el valor de un campo indicado
    def __str__(self):
	    cadena=str(self.username)
	    return cadena
		
    class Admin:
	    list_display = ('username')
#-----------------
    class Meta:
        managed = False
        db_table = 'auth_user'


class AuthUserGroups(models.Model):
    user_id = models.ForeignKey(AuthUser)
    group_id = models.ForeignKey(AuthGroup)

    class Meta:
        managed = False
        db_table = 'auth_user_groups'
        unique_together = (('user_id', 'group_id'),)


class AuthUserUserPermissions(models.Model):
    user_id = models.ForeignKey(AuthUser)
    permission_id = models.ForeignKey(AuthPermission)

    class Meta:
        managed = False
        db_table = 'auth_user_user_permissions'
        unique_together = (('user_id', 'permission_id'),)


class DjangoAdminLog(models.Model):
    action_time = models.DateTimeField()
    object_id = models.TextField(blank=True, null=True)
    object_repr = models.CharField(max_length=200)
    action_flag = models.SmallIntegerField()
    change_message = models.TextField()
    content_type = models.ForeignKey('DjangoContentType', blank=True, null=True)
    user = models.ForeignKey(AuthUser)

    class Meta:
        managed = False
        db_table = 'django_admin_log'


class DjangoContentType(models.Model):
    app_label = models.CharField(max_length=100)
    model = models.CharField(max_length=100)

    class Meta:
        managed = False
        db_table = 'django_content_type'
        unique_together = (('app_label', 'model'),)


class DjangoMigrations(models.Model):
    app = models.CharField(max_length=255)
    name = models.CharField(max_length=255)
    applied = models.DateTimeField()

    class Meta:
        managed = False
        db_table = 'django_migrations'


class DjangoSession(models.Model):
    session_key = models.CharField(primary_key=True, max_length=40)
    session_data = models.TextField()
    expire_date = models.DateTimeField()

    class Meta:
        managed = False
        db_table = 'django_session'


class ActividadUsuario(models.Model):
    auth_user = models.ForeignKey('AuthUser')
    actividad = models.ForeignKey(Actividad, db_column='Actividad_id')  # Field name made lowercase.


    class Meta:
        managed = False
        db_table = 'Actividad_Usuario'
