from django.contrib import admin
from Contenido.models import *


class clsAdmActUsr(admin.ModelAdmin):
    #fields = ['actividad', 'auth_user']# Ordenar Visualizacion
    list_display = ('id','customName','actividad','precio','estadio')
    list_filter = ['auth_user']
    search_fields = ['auth_user']
    def id(self, obj):
        # in this context, obj is the Manager instance for this line item
        return obj.pk    
    def customName(self, obj):
            # in this context, obj is the Manager instance for this line item
            return obj.auth_user
    customName.admin_order_field  = 'auth_user'
    customName.short_description = 'Nombre de Usuario'  #Renames column head

    def precio(self, obj):
        # in this context, obj is the Manager instance for this line item
        return obj.actividad.precio

    precio.admin_order_field  = 'precio'
    precio.short_description = 'Precio Actividad'  #Renames column head


    def estadio(self, obj):
        # in this context, obj is the Manager instance for this line item
        obj = Estadio()
        obj.nombre_estadio
        
        return obj

    estadio.admin_order_field  = 'nombre_estadio'
    estadio.short_description = 'Estadio'  #Renames column head


class BookAdmin(admin.ModelAdmin):
    model = AuthUser
    list_display = ['actividad', 'get_name', ]

    def get_name(self, obj):
        return obj.AuthUser.username
    get_name.admin_order_field  = 'username'  #Allows column order sorting
    get_name.short_description = 'Author Name'  #Renames column head



#Estadio
class clsAdmEstadio(admin.ModelAdmin):
    #fields = ['actividad', 'auth_user']# Ordenar Visualizacion
    list_display = ('pk','nombre_estadio')
    list_filter = ['nombre_estadio']
    search_fields = ['nombre_estadio']
#FinEstadio

#Actividad
class clsAdmActividad(admin.ModelAdmin):
    #fields = ['actividad', 'auth_user']# Ordenar Visualizacion
    list_display = ('id','customName','precio')
    list_filter = ['nombre_actividad']
    search_fields = ['nombre_actividad']
   

    def id(self, obj):
        # in this context, obj is the Manager instance for this line item
        return obj.pk
    def customName(self, obj):
            # in this context, obj is the Manager instance for this line item
            return obj.nombre_actividad
    customName.admin_order_field  = 'nombre_actividad'
    customName.short_description = 'Nombre de Actividad'  #Renames column head
#Actividad
#Actividad ESTADIO
class clsAdmActEst(admin.ModelAdmin):
    #fields = ['actividad', 'auth_user']# Ordenar Visualizacion
    list_display = ('id','customName','actividad')
    list_filter = ['actividad','estadio']
    search_fields = ['actividad','estadio']
   

    def id(self, obj):
        # in this context, obj is the Manager instance for this line item
        return obj.pk
    def customName(self, obj):
            # in this context, obj is the Manager instance for this line item
            return obj.estadio
    customName.admin_order_field  = 'estadio'
    customName.short_description = 'Nombre del Estadio'  #Renames column head
#Actividad ESTADIO


# Register your models here.
admin.site.register(Actividad,clsAdmActividad)
admin.site.register(Estadio,clsAdmEstadio)
admin.site.register(ActividadEstadio,clsAdmActEst)
admin.site.register(ActividadUsuario,clsAdmActUsr)

